package com.galvanize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class Algorithm {

    public boolean allEqual(String input){
        boolean theSame = false;
        input = input.toLowerCase();

        if (input.equals("") || input.length()==0){
            return theSame;
        }else{
            for (int i = 0; i < input.length(); i++) {
                if (input.charAt(0) != input.charAt(i)){
                    theSame = false;
                    return theSame;
                }else{
                    theSame = true;
                }
            }

        }

        return theSame;
    }

    public long letterCount(String input){
        long sumTotal = 0;
        input = input.toLowerCase();


        for (int i = 0; i < input.length(); i++) {


            for (int j = 0; j < input.length(); j++) {
                if(input.charAt(i) == input.charAt(j)){
                    sumTotal++;
                }
            }
        }
        return sumTotal;
    }
}
